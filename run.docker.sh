NAME=user-service
if ! [ "$(sudo docker images | grep $NAME)" ]; then 
    sudo docker build -t $NAME .
fi
sudo docker run --name $NAME -p 5080:5080 -d $NAME
